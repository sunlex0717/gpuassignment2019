#include <cuda_runtime.h>
#include "CImg.h"
#include <iostream>
#include "kernel.h"
#include "kernelcpu.h"
#define BLOCKSIZE 32
using namespace std;
using namespace cimg_library;


int compute_diff(unsigned char * res_cpu, unsigned char * res_gpu, unsigned long size){
  int res = 0;
  for(int i = 0;i < size; i++){
    res += res_cpu[i] - res_gpu[i];
  }
  return res;
}

int main()
{
    //load image
    CImg<unsigned char> src("cat2.jpg"); // we use cat2.jpg to grade
    int width = src.width();
    int height = src.height();
    unsigned long size = src.size();

    //create pointer to image
    unsigned char *h_src = src.data();
    
    CImg<unsigned char> dst(width, height, 1, 1);
    unsigned char *h_dst = dst.data();

    unsigned char *d_src;
    unsigned char *d_dst;

    cudaEvent_t start; // to record processing time
    cudaEvent_t stop;
    float msecTotal;


    std::cout << "Start CPU processing" << std::endl;
    // create and start timer
    cudaEventCreate(&start);
    cudaEventRecord(start, NULL); 
    // hisgram cpu
    unsigned char *cpu_ref = new unsigned char [width*height];
    rgb2gray_cpu(h_src,cpu_ref,width,height);
    // stop and destroy timer
    cudaEventCreate(&stop);
    cudaEventRecord(stop, NULL);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecTotal, start, stop);
    float cpu_time = msecTotal;

    std::cout <<"CPU processing time: " << cpu_time << " ms" <<std::endl;

    
    std::cout << "Start GPU processing" << std::endl;
    // create and start timer
    cudaEventCreate(&start);
    cudaEventRecord(start, NULL); 

    cudaMalloc((void**)&d_src, size);
    cudaMalloc((void**)&d_dst, width*height*sizeof(unsigned char));

    cudaMemcpy(d_src, h_src, size, cudaMemcpyHostToDevice);

    //launch the kernel
    dim3 blkDim (BLOCKSIZE, BLOCKSIZE, 1);
    dim3 grdDim ((width + BLOCKSIZE-1)/BLOCKSIZE, (height + BLOCKSIZE-1)/BLOCKSIZE, 1);
    rgb2gray<<<grdDim, blkDim>>>(d_src, d_dst, width, height);


    // add other three kernels here
    // clock starts -> copy data to gpu -> kernel1 -> kernel2->kernel3->kernel 4 ->copy result to cpu -> clock stops

    //wait until kernel finishes
    cudaDeviceSynchronize();

    
    //copy back the result to CPU
    cudaMemcpy(h_dst, d_dst, width*height, cudaMemcpyDeviceToHost);

    cudaEventCreate(&stop);
    cudaEventRecord(stop, NULL);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecTotal, start, stop);
    float gpu_time = msecTotal;
    

    
    int res = compute_diff(cpu_ref,h_dst,width*height);

    cudaFree(d_src);
    cudaFree(d_dst);

    std::cout << "diff cpu and gpu " << res <<std::endl; // do not change this
    std::cout <<"CPU processing time: " << gpu_time << " ms" <<std::endl; // do not change this

    //you need to save your final output, we need to measure the correctness of your program
    //read test.cpp to learn how to save a image
    //smoothing_gpu.save("smoothing_gpu.jpg"); 

    return 0;
}